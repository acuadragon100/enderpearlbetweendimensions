 package net.acuadragon100.enderpearlbetweendimensions;

 import net.fabricmc.api.ModInitializer;
 import net.fabricmc.loader.api.FabricLoader;
 import net.minecraft.block.BlockState;
 import net.minecraft.entity.Entity;
 import net.minecraft.server.network.ServerPlayerEntity;
 import net.minecraft.server.world.ServerWorld;
 import net.minecraft.util.math.Vec3d;

 public class EnderPearlBetweenDimensions implements ModInitializer {

	 public static final String modid = "enderpearlbetweendimensions";

	@Override
	public void onInitialize() {
		// This code runs as soon as Minecraft is in a mod-load-ready state.
		// However, some things (like resources) may still be uninitialized.
		// Proceed with mild caution;
		EPBDLogger.info("EnderPearlBetweenDimensions mod loaded! Ender Pearls now work between dimesions!");

		if (!isDimensionsAPIInstalled()) {
			EPBDLogger.warn("Fabric API was not detected!");
			EPBDLogger.warn("This means that other entities cannot be teleported between dimensions using Ender Pearls!");
			EPBDLogger.warn("This most likely won't cause any major issues, but I recommend installing Fabric API regardless, just in case some other mod or command block contraption allows entities to use ender pearls.");
		}
	}

	public static boolean isDimensionsAPIInstalled() {
		return FabricLoader.getInstance().isModLoaded("fabric-dimensions-v1");
	}

	 public static boolean isImmersivePortalsInstalled() {
		 return FabricLoader.getInstance().isModLoaded("imm_ptl_core");
	 }

	public static Entity teleportEntityBetweenDims(Entity entity, ServerWorld targetWorld, Vec3d targetPos) {
		Entity newEntity = entity;
		if (entity instanceof ServerPlayerEntity player) {
			player.teleport(
					targetWorld, targetPos.getX(), targetPos.getY(), targetPos.getZ(),
					player.getYaw(), player.getPitch()
			);
			return player;
		}
		if (isDimensionsAPIInstalled()) {
			newEntity = FabricDimensionsHook.teleportWithFabricDimensions(entity, targetWorld, targetPos);
		}
		return newEntity;
	}

	public static boolean isImmersivePortalsPortal(BlockState block) {
		if (isImmersivePortalsInstalled()) {
			return ImmersivePortalsHook.isImmersivePortalsPortal(block);
		}
		return false;
	}
}
