package net.acuadragon100.enderpearlbetweendimensions;

import net.fabricmc.fabric.api.dimension.v1.FabricDimensions;
import net.minecraft.entity.Entity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.TeleportTarget;

public class FabricDimensionsHook {

	public static Entity teleportWithFabricDimensions(Entity entity, ServerWorld targetDim, Vec3d targetPos) {
		return FabricDimensions.teleport(entity, targetDim, new TeleportTarget(targetPos, entity.getVelocity(), entity.getYaw(), entity.getPitch()));
	}

}
