package net.acuadragon100.enderpearlbetweendimensions.mixin;

import com.llamalad7.mixinextras.injector.ModifyReturnValue;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;

import java.util.UUID;

@Mixin(ProjectileEntity.class)
public abstract class MixinProjectileEntity extends Entity {

	@Shadow
	private UUID ownerUuid;

	@Shadow @Nullable private Entity owner;

	public MixinProjectileEntity(EntityType<?> type, World world) {
		super(type, world);
	}

	@ModifyReturnValue(method = "getOwner", at = @At("RETURN"))
	public Entity enderPearlThroughPortalsSearchAllDimensionsForTheOwner(Entity orgEntity) {
		if (orgEntity != null) return orgEntity;
		if (this.ownerUuid != null && this.getWorld() instanceof ServerWorld) {
			Entity entity = getServer().getPlayerManager().getPlayer(ownerUuid);
			if (entity != null) {
				this.owner = entity;
				return owner;
			}
			for (ServerWorld world : this.getServer().getWorlds()) {
				entity = world.getEntity(this.ownerUuid);
				if (entity != null) {
					this.owner = entity;
					return this.owner;
				}
			}
		}
		return null;
	}

}
