package net.acuadragon100.enderpearlbetweendimensions.mixin;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import com.llamalad7.mixinextras.injector.WrapWithCondition;
import com.llamalad7.mixinextras.sugar.Local;
import com.llamalad7.mixinextras.sugar.ref.LocalRef;
import io.github.apace100.origins.entity.EnderianPearlEntity;
import net.acuadragon100.enderpearlbetweendimensions.EnderPearlBetweenDimensions;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.projectile.thrown.ThrownItemEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(EnderianPearlEntity.class)
public abstract class MixinEnderianPearlEntity extends ThrownItemEntity {
	public MixinEnderianPearlEntity(EntityType<? extends ThrownItemEntity> entityType, World world) {
		super(entityType, world);
	}

	@Inject(method = "moveToWorld", at = @At("HEAD"), cancellable = true)
	public void enderPearlBetweenDimensionsPreventDimChangeDestruction(ServerWorld destination, CallbackInfoReturnable<Entity> info) {
		info.setReturnValue(super.moveToWorld(destination));
		info.cancel();
	}

	@Inject(method = "onCollision", at = @At("HEAD"), cancellable = true)
	protected void enderPearlBetweenDimensionsNoCollisionWithPortal(HitResult hitResult, CallbackInfo info) {
		if (hitResult instanceof BlockHitResult && hitResult.getType().equals(HitResult.Type.BLOCK)) {
			BlockPos pos = ((BlockHitResult)hitResult).getBlockPos();
			if (EnderPearlBetweenDimensions.isImmersivePortalsPortal(getWorld().getBlockState(pos))) {
				info.cancel();
			}
		}
	}

	@ModifyExpressionValue(
			method = "onCollision",
			at = @At(
					value = "INVOKE",
					target = "Lnet/minecraft/server/network/ServerPlayerEntity;getWorld()Lnet/minecraft/world/World;"
			)
	)
	public World enderPearlBetweenDimensionsAlwaysAllowTeleport(World world) {
		return this.getWorld();
	}

	@WrapWithCondition(method = "onCollision", at = @At(
			value = "INVOKE", target = "Lnet/minecraft/entity/Entity;requestTeleport(DDD)V"
	))
	protected boolean enderPearlBetweenDimensionsTeleportToOtherDim(
			Entity entity, double destX, double destY, double destZ, @Local LocalRef<Entity> entityVar
	) {
		if (isSameWorld(entity)) {
			return true;
		}
		Entity newEntity = EnderPearlBetweenDimensions.teleportEntityBetweenDims(
				entity, (ServerWorld) getWorld(), new Vec3d(destX, destY, destZ)
		);
		if (newEntity != entity) {
			entityVar.set(newEntity);
		}
		return false;
	}

	@Unique
	private boolean isSameWorld(Entity entity) {
		return entity.getWorld().getRegistryKey() == this.getWorld().getRegistryKey();
	}

	//Might be necessary in future version of Origins.
	/*@WrapWithCondition(method = "onCollision", at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/server/network/ServerPlayerEntity;requestTeleportAndDismount(DDD)V"
	))
	protected boolean enderPearlBetweenDimensionsTeleportRidingPlayerToOtherDim(
			ServerPlayerEntity player, double destX, double destY, double destZ
	) {
		if (isSameWorld(player)) {
			return true;
		}
		player.teleport((ServerWorld) getWorld(), destX, destY, destZ, player.getYaw(), player.getPitch());
		return false;
	}*/
}
