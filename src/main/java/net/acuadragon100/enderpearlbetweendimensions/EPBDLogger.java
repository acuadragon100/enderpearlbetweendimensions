package net.acuadragon100.enderpearlbetweendimensions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EPBDLogger {

	public static final Logger logger = LogManager.getLogger(EnderPearlBetweenDimensions.modid);

	public static void info(String info) {
		logger.info(info);
	}

	public static void warn(String warn) {
		logger.warn(warn);
	}
}
