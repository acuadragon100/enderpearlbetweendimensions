package net.acuadragon100.enderpearlbetweendimensions;

import net.minecraft.block.BlockState;
import qouteall.imm_ptl.core.portal.PortalPlaceholderBlock;

public class ImmersivePortalsHook {

	public static boolean isImmersivePortalsPortal(BlockState state) {
		return state.getBlock() instanceof PortalPlaceholderBlock;
	}

}
